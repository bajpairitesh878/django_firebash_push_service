django_firebash_push_service

Requirements

python >= 3.6
django >= 3
djangorestframework
requests

Installable App

This app can be installed and used in your django project by:

$ pip install django-firebash-push-service
Edit your settings.py file to include 'dj_push' in the INSTALLED_APPS listing.

INSTALLED_APPS = [
    ...

    'dj_push',
]
Edit your project urls.py file to import the URLs:

url_patterns = [
    ...

    path('api/v1/push/', include('dj_push.urls')),
]
Finally, add the models to your database:

$ ./manage.py makemigrations dj_push
The "project" Branch
The master branch contains the final code for the PyPI package. There is also a project branch which shows the "before" case -- the Django project before the app has been removed.

$ Guide

First save device token 

api_endpoint = api/vi/push/device/token/create
payload = {
    "token": device_token,
    "device_os: "android"
}

after save token use this method for send notification
notice = NotificationSend(user=user_id, message=message, title=title)
response_msg = notice.send_device_notification()



Docs & Source
Article: https://pypi.org/user/bajpairitesh878/
Source: https://gitlab.com/bajpairitesh878/django_firebash_push_service
PyPI: https://pypi.org/user/bajpairitesh878/