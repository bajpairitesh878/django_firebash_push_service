from django.apps import AppConfig


class DjPushConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'dj_push'
    verbose_name = 'Push Notification Device Token'
